
package Controllers;
import Models.Productos;
import Models.dbProducto;
import Views.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
//import java.sql.Date;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author quier
 */
public class TestDb implements ActionListener {
    
    private Productos pro = new Productos();
    private dbProducto db = new dbProducto();
    private dlgVista vista = new dlgVista(new JFrame(), true);
    private static int sw = 0;
    
    public TestDb(Productos pro, dbProducto db, dlgVista vista) {
        this.pro = pro;
        this.db = db;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnBuscarActivo.addActionListener(this);
        vista.btnBuscarNoActivo.addActionListener(this);
        vista.btnInsertarGuardar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
        updateTable(vista.jtbActive, 1);
        updateTable(vista.jtbNoActive, 0);
    }
    
    public static void main(String[] args) {
        Productos pro = new Productos();
        dbProducto db = new dbProducto();
        dlgVista vista = new dlgVista(new JFrame(), true);
        
        TestDb controlador = new TestDb(pro, db, vista);
        controlador.iniciarVista();
    }
    
    public void iniciarVista() {
        this.vista.setSize(600, 800);
        this.vista.setVisible(true);
        this.vista.setTitle("Pepe");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo)
        {
            limpiar();
            vista.txtCodigo.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.jDateChooser.setEnabled(true);
            vista.btnInsertarGuardar.setText("Insertar | Guardar");
            vista.btnInsertarGuardar.setEnabled(false);
            vista.btnDeshabilitar.setEnabled(false);
            sw = 1;
        }
        
        if(e.getSource() == vista.btnInsertarGuardar) {
            
            try {
                if(db.isExiste(vista.txtCodigo.getText()) == true && sw != 2)
                {
                    JOptionPane.showMessageDialog(vista, "Ya existe el código introducido");
                    return;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            
            setAttributes();
                
            try {
                
                if(sw == 1)
                {
                    db.insertar(this.pro);
                    JOptionPane.showMessageDialog(vista, "Se logró insertar");
                }
                else
                {
                    //System.out.println("Updating");
                    db.actualizar(this.pro);
                    JOptionPane.showMessageDialog(vista, "Se logró actualizar");
                }
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            limpiar();
            vista.txtCodigo.setEnabled(true);
            vista.txtNombre.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.jDateChooser.setEnabled(false);
            vista.btnInsertarGuardar.setEnabled(false);
            vista.btnInsertarGuardar.setText("Insertar | Guardar");
            vista.btnDeshabilitar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnHabilitar) {
            //setAttributes();
            try {
                this.pro.setStatus(1);
                db.habilitar(this.pro);
                //vista.txtStatus.setText(Integer.toString(this.pro.getStatus()));
                JOptionPane.showMessageDialog(vista, "Se logró habilitar");
            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "Errorsdsdf: " + ex.getMessage());
                return;
            }
            limpiar();
            vista.btnHabilitar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnDeshabilitar) {
            setAttributes();
            try {
                this.pro.setStatus(0);
                db.deshabilitar(this.pro);
                //vista.txtStatus.setText(Integer.toString(this.pro.getStatus()));
                JOptionPane.showMessageDialog(vista, "Se logró deshabilitar");
                
                // We clean variables and block fields
                limpiar();
                vista.txtCodigo.setEnabled(true);
                vista.txtNombre.setEnabled(false);
                vista.txtPrecio.setEnabled(false);
                vista.jDateChooser.setEnabled(false);
                vista.btnInsertarGuardar.setEnabled(false);
                vista.btnInsertarGuardar.setText("Insertar | Guardar");
                vista.btnDeshabilitar.setEnabled(false);
            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
        }
        
        if(e.getSource() == vista.btnBuscarActivo) {
            
            try {
                if("".equalsIgnoreCase(vista.txtCodigo.getText()))
                {
                    throw new IllegalArgumentException("Tienes que escribir un código");
                }
                else if(Integer.parseInt(vista.txtCodigo.getText()) <= 0)
                    throw new IllegalArgumentException("No puede haber un código negativo o cero");
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            
            try {
                this.pro.setCodigo(vista.txtCodigo.getText());
                Object objPro = db.buscar(this.pro.getCodigo(), 2);
                Productos pro = (Productos) objPro;
                
                if((db.isExiste(vista.txtCodigo.getText()) == true) && (pro.getStatus() == 1))
                {
                    //vista.btnHabilitar.setEnabled(true);
                    vista.txtCodigo.setEnabled(false);
                    vista.txtNombre.setEnabled(true);
                    vista.txtPrecio.setEnabled(true);
                    vista.jDateChooser.setEnabled(true);
                    vista.btnInsertarGuardar.setEnabled(true);
                    vista.btnDeshabilitar.setEnabled(true);
                    
                    vista.btnInsertarGuardar.setText("Actualizar");
                    sw = 2;
                }
                else if((db.isExiste(vista.txtCodigo.getText()) == true) && (pro.getStatus() == 0))
                {
                    JOptionPane.showMessageDialog(vista , "Existe, pero se encuentra deshabilitado");
                    vista.btnHabilitar.setEnabled(false);
                    return;
                }
                else if(sw == 0)
                {
                    // No existe el código
                    JOptionPane.showMessageDialog(vista, "¡Disponible!");
                    vista.btnHabilitar.setEnabled(false);
                    
                    //vista.btnInsertarGuardar.setText("Insertar");
                    return;
                }
                else
                {
                    // No existe el código
                    JOptionPane.showMessageDialog(vista, "¡Disponible!");
                    vista.btnHabilitar.setEnabled(false);
                    //limpiar();
                    vista.txtCodigo.setText(this.pro.getCodigo());
                    vista.btnDeshabilitar.setEnabled(false);
                    vista.btnInsertarGuardar.setText("Insertar");
                    vista.txtCodigo.setEnabled(false);
                    vista.btnInsertarGuardar.setEnabled(true);
                    sw = 1;
                    return;
                }
                
                this.pro.setIdProducto(pro.getIdProducto());
                this.pro.setCodigo(pro.getCodigo());
                this.pro.setNombre(pro.getNombre());
                this.pro.setPrecio(pro.getPrecio());
                this.pro.setFecha(pro.getFecha());
                this.pro.setStatus(pro.getStatus());
                
                vista.txtIdProducto.setText(Integer.toString(this.pro.getIdProducto()));
                vista.txtCodigo.setText(this.pro.getCodigo());
                vista.txtNombre.setText(this.pro.getNombre());
                vista.txtPrecio.setText(Float.toString(this.pro.getPrecio()));
                
                String fecha = this.pro.getFecha();
                SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
                
                Date date = dateFormat.parse(fecha);
                vista.jDateChooser.setDate(date);
            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
        }
        
        if(e.getSource() == vista.btnBuscarNoActivo) {
            try {
                this.pro.setCodigo(vista.txtCodigoNo.getText());
                Object objPro = db.buscar(this.pro.getCodigo(), 2);
                Productos pro = (Productos) objPro;
                
                if((db.isExiste(vista.txtCodigoNo.getText()) == true) && (pro.getStatus() == 0))
                    vista.btnHabilitar.setEnabled(true);
                else if(pro.getStatus() == 1)
                {
                    JOptionPane.showMessageDialog(vista, "Estado disponible. Solo no disponibles");
                    vista.btnHabilitar.setEnabled(false);
                    return;
                }                  
                else
                {
                    JOptionPane.showMessageDialog(vista, "No existe el código");
                    vista.btnHabilitar.setEnabled(false);
                    return;
                }
                
                this.pro.setIdProducto(pro.getIdProducto());
                this.pro.setCodigo(pro.getCodigo());
                this.pro.setNombre(pro.getNombre());
                this.pro.setPrecio(pro.getPrecio());
                this.pro.setFecha(pro.getFecha());
                this.pro.setStatus(pro.getStatus());
                
                vista.txtCodigoNo.setText(this.pro.getCodigo());
                vista.txtNombreNo.setText(this.pro.getNombre());

            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
        }
        
        if(e.getSource() == vista.btnLimpiar)
            limpiar();
        
        if(e.getSource() == vista.btnCancelar)
        {
            limpiar();
            vista.txtCodigo.setEnabled(true);
            vista.txtNombre.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.jDateChooser.setEnabled(false);
            vista.btnInsertarGuardar.setText("Insertar | Guardar");
            vista.btnInsertarGuardar.setEnabled(false);
            vista.btnDeshabilitar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnCerrar)
        {
            int option = JOptionPane.showConfirmDialog(vista, "¿Seguro que quieres salir?", "Confirmar salida", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION)
            {
                vista.dispose();
                System.exit(0);
            }
        }
        
        updateTable(vista.jtbActive, 1);
        updateTable(vista.jtbNoActive, 0);
    }
    
    public void setAttributes() {
        try {
            pro.setCodigo(vista.txtCodigo.getText());
            pro.setNombre(vista.txtNombre.getText());
            pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
            pro.setStatus(0);
            
            // fecha
            // Assuming "pro" is an instance of your class and "vista" is the view object.
            LocalDate selectedDate = vista.jDateChooser.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = selectedDate.format(formatter);
            this.pro.setFecha(formattedDate);
        }
        catch(Exception e) {
            JOptionPane.showMessageDialog(vista, "Error: " + e.getMessage());
            return;
        }
    }
    
    public void updateTable(JTable table, int status) {
        try {
            ArrayList<Productos> productosList = db.listar();

            // Create the table model with column names
            String[] columnNames = {"ID", "Código", "Nombre", "Precio", "Fecha", "Status"};
            DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0);

            // Populate the table model with data
            for (Productos producto : productosList) {
                Object[] rowData = {
                    producto.getIdProducto(),
                    producto.getCodigo(),
                    producto.getNombre(),
                    producto.getPrecio(),
                    producto.getFecha(),
                    producto.getStatus()
                };
                if((int)rowData[5] == status)
                    tableModel.addRow(rowData);
            }

            // Set the table model to the existing JTable component
            table.setModel(tableModel);

        } catch (Exception e) {
            // Handle the exception appropriately
            e.printStackTrace();
        }
    }
    
    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtCodigoNo.setText("");
        vista.jDateChooser.setDate(null);
        vista.txtIdProducto.setText("");
        vista.txtNombre.setText("");
        vista.txtNombreNo.setText("");
        vista.txtPrecio.setText("");
    }
    
}
