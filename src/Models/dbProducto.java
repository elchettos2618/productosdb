
package Models;

import java.util.ArrayList;

/**
 *
 * @author quier
 */
public class dbProducto extends DbManejador implements dbPersistencia {

    @Override
    public void insertar(Object objeto) throws Exception {
        
        // Esto es una promoción, se fuerza el cambio de dato, por ejemplo, de float a int o viceversa
        Productos pro = new Productos();
        pro = (Productos) objeto;
        
        String consulta = "";
        
        // Manera de insertar código en una consulta de mejor manera, usando (?)
        consulta = "INSERT INTO productos(codigo, nombre, fecha, precio, status) VALUES(?,?,?,?,?)";
        
        if(this.conectar()) {
            System.out.println("Se conectó");
            this.sqlConsulta = conexion.prepareStatement(consulta);
            
            // Se asignan los valores
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.setString(2, pro.getNombre());
            this.sqlConsulta.setString(3, pro.getFecha());
            this.sqlConsulta.setFloat(4, pro.getPrecio());
            this.sqlConsulta.setInt(5, 1); // se queda por defecto con 1

            this.sqlConsulta.executeUpdate();
            this.desconectar();
            System.out.println("Se agregó con éxito");
        }
        else {
            System.out.println("No fue posible conectarse");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        
        try {
            if(!isExiste(pro.getCodigo()))
                throw new IllegalArgumentException("No se puede actualizar un código que no existe...");
        }
        catch(IllegalArgumentException e) {
            System.err.println("Error: " + e.getMessage());
            return;
        }
        
        String consulta = "UPDATE productos SET nombre = ?, PRECIO = ?, "
                + "fecha = ? WHERE codigo = ?";
        
        if(this.conectar()) {
            try {
                System.out.println("Se conectó");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                
                // Se asignan los valores
                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setFloat(2, pro.getPrecio());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setString(4, pro.getCodigo());
            
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(Exception e) {
                System.err.println("Error" + e.getMessage());
            }
        }
        else {
            System.err.println("No se logró conectar");
        }
    }

    @Override
    public void habilitar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        
        String consulta = "UPDATE productos SET status = 1 WHERE codigo = ?";
        
        if(this.conectar()) {
            try {
                System.out.println("Se conectó");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                
                // Se asignan los valores
                this.sqlConsulta.setString(1, pro.getCodigo());
            
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(Exception e) {
                System.err.println("Error: " + e.getMessage());
            }
        }
        else {
            System.err.println("No se logró conectar");
        }
    }

    @Override
    public void deshabilitar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        
        String consulta = "UPDATE productos SET status = 0 WHERE codigo = ?";
        
        if(this.conectar()) {
            try {
                System.out.println("Se conectó");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                
                // Se asignan los valores
                this.sqlConsulta.setString(1, pro.getCodigo());
            
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(Exception e) {
                System.err.println("Error" + e.getMessage());
            }
        }
        else {
            System.err.println("No se logró conectar");
        }
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        Productos pro = new Productos();
        
        if(this.conectar()) {
            String consulta = "SELECT * FROM productos WHERE codigo = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            
            // Asignar valores
            this.sqlConsulta.setString(1, codigo);
            
            // Hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            
            // Se checa si el registro del código existe
            if(this.registros.next()) {
                if (this.registros.getString("codigo") != null)
                    return true;
                else
                    return false;
            }
        }
        this.desconectar();
        return false;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        
        if(this.conectar()) {
            String consulta = "SELECT * FROM productos ORDER BY codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            // Sacar los registros
            while(this.registros.next()) {
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setNombre(this.registros.getString("nombre"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                lista.add(pro);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public ArrayList listar(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(String codigo, int status) throws Exception {
        Productos pro = new Productos();
        
        if(this.conectar()) {
            String consulta = "SELECT * FROM productos WHERE codigo = ? AND status = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            
            // Asignar valores
            this.sqlConsulta.setString(1, codigo);
            this.sqlConsulta.setInt(2, status);
            
            // Si status es 2, no se da parametro
            if(status == 2)
            {
                consulta = "SELECT * FROM productos WHERE codigo = ?";
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, codigo);
            }
            
            // Hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            
            // Sacar los registros
            if(this.registros.next()) {
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setNombre(this.registros.getString("nombre"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
            }
        }
        this.desconectar();
        return pro;
    }
    
}
