
package Models;
import java.util.ArrayList;

/**
 *
 * @author quier
 */
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objeto) throws Exception;
    public void habilitar(Object objeto) throws Exception;
    public void deshabilitar(Object objeto) throws Exception;
    
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listar(String criterio) throws Exception;
    
    public Object buscar(int id) throws Exception;
    public Object buscar(String codigo, int status) throws Exception;
    
    
} 
